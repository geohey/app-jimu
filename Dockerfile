FROM registry.cn-beijing.aliyuncs.com/geohey/node:v8.12.0

WORKDIR /var/geohey/apps/app-jimu

COPY ./ ./

#EXPOSE 9109
EXPOSE 8887

CMD [ "node", "app.js" ]