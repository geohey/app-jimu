var Koa = require( 'koa' );
var static = require( 'koa-static' );
var router = require( 'koa-router' )();
var views = require( 'koa-views' );
var swig = require( 'swig' );
var convert = require( 'koa-convert' );
var bodyParser = require( 'koa-bodyparser' );
var path = require( 'path' );
var config = require( './config' );
var pages = require( './routes/pages' );
var app = new Koa();

app.use( bodyParser( {
    jsonLimit: '1mb',
    formLimit: '1mb'
} ) );

app.use( convert( static('./static' ) ) );
app.use( views( path.join( __dirname, '/views' ), { map: { html: 'swig' } } ) );
router.use( pages.routes(), pages.allowedMethods() );
app.use( router.routes(), router.allowedMethods() );

var server = app.listen( config.port, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log( 'app listening at http://%s:%s', host, port );
} );


module.exports = app;
