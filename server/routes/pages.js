const router = require( 'koa-router' )();
router.get( '/', async function ( ctx ) {
    await ctx.render( 'index', {} );
} );

module.exports = router;
