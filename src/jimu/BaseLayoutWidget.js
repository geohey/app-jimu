import jimuUtils from '@/jimu/utils/utils.js'
import BaseWidget from './BaseWidget.js'
export default {
	name: 'BaseLayoutWidget',
	mixins: [BaseWidget],
	data() {
		return {
			widgetList: [],  // 存储所有子组件对象
			widgets: []      // 所有子组件配置
		}
	},
	created: function () {
		this.widgets = this.options.widgets ? this.options.widgets : []
		this._subscribe()
		this._createWidgets()
	},

	methods: {
		_subscribe: function () {
			this.bus.$on(this.event.OPEN_WIDGET, this._openWidget)
			this.bus.$on(this.event.CLOSE_WIDGET, this._closeWidget)
		},

		/* 创建子组件 */
		_createWidgets: async function () {
			let len = this.widgets.length
			for (let i = 0; i < len; i++) {
				if (this.widgets[i].on) {
					await this._addWidget(this.widgets[i])
				}
			}
		},

		/* 打开组件 */
		_openWidget: function (id) {
			let config = this._getConfig(id)
			if (!this._hasWidget(id) && config) {
				this._addWidget(config)
			}
		},

		/* 关闭组件 */
		_closeWidget: function (id) {
			let len = this.widgetList.length
			for (let i = 0; i < len; i++) {
				if (this.widgetList[i].id === id) {
					this.widgetList.splice(i, 1)
					return
				}
			}
		},

		/* 判断是否是自身管理的组件 */
		_hasWidget: function (id) {
			return this.widgetList.some(widget => widget.id === id)
		},

		/* 获取指定子组件配置 */
		_getConfig: function (id) {
			let config = null
			this.widgets.forEach(widget => {
				if (widget.id === id) {
					config = widget
				}
			})
			return config
		},

		/* 根据配置添加组件 */
		_addWidget: async function (config) {
			let module = await import("@/" + config.uri + ".vue")
			let widgetObj = jimuUtils.deepClone(module.default)
			let dataObj = {}
			if (widgetObj.data) {
				dataObj = widgetObj.data()
			}
			widgetObj.id = config.id
			widgetObj.positionStyle = this._getPositionStyle(config.position)
			
			// 组件data对象混入外部属性
			Object.assign(dataObj, {
				options: config
			})
			widgetObj.data = () => jimuUtils.deepClone(dataObj)
			this.widgetList.push(widgetObj)
			this.childWidgetCreate(config)
		},

		/* 生成位置信息样式 */
		_getPositionStyle: function (position) {
			let styleStr = ""
			if (position) {
				styleStr += (position.top || position.top === 0) ? ("top:" + position.top + "px;") : ""
				styleStr += (position.left || position.left === 0) ? ("left:" + position.left + "px;") : ""
				styleStr += (position.bottom || position.bottom === 0) ? ("bottom:" + position.bottom + "px;") : ""
				styleStr += (position.right || position.right === 0) ? ("right:" + position.right + "px;") : ""
				styleStr += (position.height || position.height === 0) ? ("height:" + position.height + "px;") : ""
				styleStr += (position.width || position.width === 0) ? ("width:" + position.width + "px;") : ""
				styleStr += (position.zIndex || position.zIndex === 0) ? ("z-index:" + position.zIndex) : ""
			}
			return styleStr
		},

		/* 内部组件创建后执行 子组件可进行重载 */
		childWidgetCreate: function (config) {
			return
		},

		/**
		 * 容器组件销毁方法 
		 * 调用此方法会导致容器内部的子组件也被销毁 
		 * 并且内部子组件无法通过事件打开
		 */ 
		close: function(){
			this.widgetList = []
			this.bus.$emit(this.event.CLOSE_WIDGET, this.options.id)
		}

	}
}
