// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import ConfigManager from './manager/ConfigManager'
import EventBus from './EventBus'
import EventType from './EventType'
import store from './store.js'
import '@/assets/styles/reset.scss'
import '@/assets/styles/jimu.scss'
import '@/assets/font/iconfont.css'
// 极海SDK
import 'geohey-javascript-sdk/dist/lib/g.css'
import G from 'geohey-javascript-sdk'
import 'geohey-javascript-sdk/dist/lib/g-canvas.min'
import 'geohey-javascript-sdk/dist/lib/g-heat.min'
import 'geohey-javascript-sdk/dist/lib/g-maps.min'
import 'geohey-javascript-sdk/dist/lib/g-fluid.min'
import 'geohey-javascript-sdk/dist/lib/g-mapviz.min'

Vue.config.productionTip = false
Vue.prototype.bus = EventBus       // 注入事件线对象
Vue.prototype.store = store        // 注入datastore对象
Vue.prototype.event = EventType    // 注入系统事件类型

// 加载配置文件
let configManager = new ConfigManager()
configManager.loadConfig()

new Vue({
	el: '#app',
	components: { App },
	template: '<App/>',
})

