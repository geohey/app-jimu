const CLOSE_WIDGET = "CLOSE_WIDGET"         // 关闭组件事件
const OPEN_WIDGET = "OPEN_WIDGET"           // 打开组件事件
const OPEN_RIGHT_PANEL = "OPEN_RIGHT_PANEL" // 打开右侧面板

export default {
	CLOSE_WIDGET,
	OPEN_WIDGET,
	OPEN_RIGHT_PANEL
}
