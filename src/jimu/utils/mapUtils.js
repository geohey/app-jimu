const mapUtils = { 
    setMapViz: function(uid, ak, map, callback){
        if(!uid || !ak || !map) return
        this._removeLegendControl(map)
        G.MapViz.get(uid, {
            "host": "https://geohey.com",
            "ak": ak,
            "tileHost": "http://{s}.geohey.com",
            "cluster": [ "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8" ]
        }, map, (mapVizData)=> {
            if(callback){
                callback(mapVizData)
            }		
            if(mapVizData.baseLayer){
                mapVizData.baseLayer.remove()
            }
            this._removeLayerControl(map)
            
        })
		
    },

    _removeLegendControl: function(map){
        let legendNodes = map._panelFrame.getElementsByClassName("g-legend")
			for(let i = 0; i < legendNodes.length;i++){
				legendNodes[i].remove()
			}
    },

    _removeLayerControl: function(map){
        let legendNodes = map._panelFrame.getElementsByClassName("g-control-layers")
        for(let i = 0; i < legendNodes.length; i++){
            legendNodes[i].remove()
        }
    }
}

export default mapUtils;