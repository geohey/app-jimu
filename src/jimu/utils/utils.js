const utils = { 
    // 生成随机key
	random32key: function(){
        var len = 32;
        var chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
        var maxPos = chars.length;
        var key = '';
        for (let i = 0; i < len; i++) {
            key += chars.charAt(Math.floor(Math.random() * maxPos));
        }
        return key
    },
    
    stripHTML: function (str){
        if (!str) {
            return str;
        }
        if (str.indexOf("<") > -1 && str.indexOf(">") > -1) {
            // this gets pretty slow if the string is long and has a < and no >
            var matchTag = /<(?:.|\s)*?>/g;
            return str.replace(matchTag, "");
        } else {
            return str;
        }
    },
    
    deepCopy: function(obj){
        return JSON.parse(JSON.stringify(obj))
    },

    deepClone: function(obj){
        let objClone = Array.isArray(obj)?[]:{};
        if(obj && typeof obj === "object"){
            for(let key in obj){
                if(obj.hasOwnProperty(key)) {
                    //判断ojb子元素是否为对象，如果是，递归复制
                    if(obj[key] && typeof obj[key] === "object"){
                        objClone[key] = this.deepClone(obj[key]);
                    }else{
                        //如果不是，简单复制
                        objClone[key] = obj[key];
                    }
                }
            }
        }
        return objClone;
    }
}

export default utils;