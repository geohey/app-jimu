const colorUtils = { 
    hex2Rgb: function(hex){
        hex = hex.replace("#", "")
        return{
            r: parseInt("0x" + hex.slice(1, 3)),
            g: parseInt("0x" + hex.slice(3, 5)),
            b: parseInt("0x" + hex.slice(5, 7))
        }
    },
    rgb2Hex: function(r, g, b){
        return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
    }
}

export default colorUtils;