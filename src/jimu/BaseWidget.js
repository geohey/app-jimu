import jimuUtils from '@/jimu/utils/utils.js'
export default {
	name: 'BaseWidget',
	data () {
		return {
			config: null,                      // 组件配置
			shareState: this.store.state,      // 组件间共享状态
			store: this.store,                 // 
			event: this.event,                 // 全局事件类型
			bus: this.bus,                     // EventBus 对象
			isShow: true                       // 组件状态
		}
	},

	created: function(){
		// 优先获取组件用户自定义配置信息
		this.config = this.options.config ? this.options.config : null
		if(!this.config){
			this._loadDefaultConfig()
		}
	},

	methods: {
		/* 加载默认配置文件 */
		_loadDefaultConfig: function() {
			if(this.options.folderUri){
				this.config = jimuUtils.deepCopy(require('@/' + this.options.folderUri + '/config.json'))
			}
		},

		/* 将内部数据共享到中心store */
		share: function(key, value){
			this.$set(this.store.state, key, value)
		},
		
		/**
		 * 关闭组件 
		 * 会触发组件生命周期的destroyed钩子
		 */ 
		close: function(){
			this.bus.$emit(this.event.CLOSE_WIDGET, this.options.id)
		}
	}
}
