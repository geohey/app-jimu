// 组件状态存储容器
const store = {
	// 组件状态存储
	state: {

	},
	isLoading: false,
	// 工具箱列表
	toolList: [],
	// 组件id
	widgetId: 10000,
	// 系统配置
	appConfig: {},
	// 系统组件配置
	widgetsConfig: [],
	// 区别开发环境生产环境
	isDebug: process.env.NODE_ENV !== 'production'
}
export default store