/**
 * 组件管理器
 */
import store from '@/jimu/store'
import jimuUtils from '@/jimu/utils/utils.js'
export default class ConfigManager {
	constructor() {
		this.subscribe();
		this.idList = []
	}

	/* 消息订阅 */
	subscribe() {

	}

	/* 加载配置项 */
	loadConfig() {
		let config = require('../config.json')
		this._praseConfig(config)
	}

	/* 解析配置文件内容 */
	_praseConfig(config) {
		this._parseAppConfig(config)
		this._setDocumentTitle()
		this._setDocumentFavicon()
		if (config.hasOwnProperty("widgets")) {
			store.widgetsConfig = this._praseWidgetsConfig(config.widgets)
		}
	}

	/* 解析应用配置 */
	_parseAppConfig(config) {
		Object.keys(config).forEach((key) => {
			if (key !== "widgets") {
				store.appConfig[key] = config[key]
			}
		})
	}

	/* 设置标题 */
	_setDocumentTitle() {
		if (store.appConfig.title) {
			document.title = jimuUtils.stripHTML(store.appConfig.title)
		}
	}

	/* 设置favicon */
	_setDocumentFavicon() {
		if (store.appConfig.favicon) {
			let link = document.querySelector("link[rel*='icon']") || document.createElement('link')
			link.type = 'image/x-icon'
			link.rel = 'shortcut icon'
			link.href = jimuUtils.stripHTML(store.appConfig.favicon)
			document.getElementsByTagName('head')[0].appendChild(link)
		}
	}

	/* 解析构造组件树配置 */
	_praseWidgetsConfig(widgetsConfig) {
		let widgetConfigList = []
		for (let i = 0; i < widgetsConfig.length; i++) {
			let msg = ""
			let formatConfig = {}
			let widget = widgetsConfig[i]
			let { uri, widgets, position, name } = widget
			let id = this._generateWidgetId(widget)
			widget.id = id
			this._setGroupList(widget)
			if (uri) {
				if (this._checkKeyUnique(id)) {
					msg = "组件id值：" + id + " 与其他组件重复"
					widgetConfigList.push(this._errorWidgetConfig(msg, position))
					continue
				}
				this.idList.push(id)
				formatConfig = {
					name: name ? name : "",
					id: id,
					on: widget.hasOwnProperty("on") ? widget.on : true,  // 默认为true
					uri: uri,
					folderUri: uri.substring(0, uri.lastIndexOf("/"))
				}
			} else {
				msg = "组件配置uri属性不存在"
				widgetConfigList.push(this._errorWidgetConfig(msg, position))
				continue
			}
			if (position) {
				formatConfig.position = this._formatPosition(position)
			}
			// 检查widgets属性正确性
			if (widgets) {
				if (widgets instanceof Array) {
					formatConfig.widgets = this._praseWidgetsConfig(widgets)
				} else {
					formatConfig.widgets = []
					msg = "子组件配置项widgets类型错误"
					formatConfig.widgets.push(this._errorWidgetConfig(msg, position))
				}
			}
			widgetConfigList.push(formatConfig)
		}
		return widgetConfigList

	}

	/* 矫正position属性格式 */
	_formatPosition(position) {
		let { top, left, right, bottom, height, width, zIndex } = position
		return {
			top: (isNaN(top) && top) ? top.replace("px", "") : top,
			left: (isNaN(left) && left) ? left.replace("px", "") : left,
			right: (isNaN(right) && right) ? right.replace("px", "") : right,
			bottom: (isNaN(bottom) && bottom) ? bottom.replace("px", "") : bottom,
			height: (isNaN(height) && height) ? height.replace("px", "") : height,
			width: (isNaN(width) && width) ? width.replace("px", "") : width,
			zIndex: zIndex
		}
	}

	/* 检查key值是否重复 */
	_checkKeyUnique(checkId) {
		return this.idList.some((id) => id === checkId)
	}

	/* 配置出错Error替代组件 */
	_errorWidgetConfig(msg, position) {
		return {
			on: true,
			uri: 'widgets/ErrorWidget/ErrorWidget',
			position: position,
			config: {
				msg: msg
			}
		}
	}

	/* 抽取出通过菜单进行管理的组件 */
	_setGroupList(config) {
		if ( config.groupKey || config.groupKey === 0) {
			let { name, id, icon } = config
			store.toolList.push(
				{
					name: name ? name : "",
					id: id ? id : "",
					icon: icon ? icon : "g-icon-tiling",
					groupKey: config.groupKey
				}
			)
		}
	}

	/* 生成组件id 配置中有id采用 没有则自动生成 */
	_generateWidgetId(config) {
		let widgetId = ""
		if (config.id) {
			widgetId = config.id
		} else {
			widgetId = "widget_" + (store.widgetId++)
		}

		return widgetId
	}
}