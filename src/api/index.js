import axios from 'axios'
import store from '@/jimu/store'
let pre = `https://geohey.com`
const ak = store.appConfig.ak
const baseUrl = {
    isochrone: '/s/isochrone/',
    publicData: '/s/public_data/',
    data: '/s/data/',
    retail: '/s/predict'
}

/* 等时圈 */
const isochrone = {
    getIsochrone(type, params) {
        let url = `${pre}${baseUrl.isochrone}${type}?ak=${ak}`
        return axios.post(url, params)
    }
}
const publicData = {
    query(uid, params) {
        let url = `${pre}${baseUrl.publicData}${uid}/query?ak=${ak}`
        return axios.post(url, params)
    }
}
const data = {
    query(uid, params) {
        let url = `${pre}${baseUrl.data}${uid}/query?ak=${ak}`
        return axios.get(url, params)
    }
}
const retail = {
	predict(params) {
		let url = `https://demo.geohey.com${baseUrl.retail}?x=${params.x}&y=${params.y}`
		return axios.get(url)
	}
}
export {
    isochrone,
    publicData,
    data,
	retail
}
